const express = require('express');
const morgan = require('morgan');
const path = require('path');
const fs = require('fs');
const app = express();
const DEBUG = process.env.DEBUG || false;

app.listen(process.env.PORT || 3000);
app.set('case sensitive routing', true);
app.use(morgan(':remote-addr - :remote-user [:date[iso]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent"'));
app.use(express.static(path.join(__dirname, 'public')));

// Home page, return 200
app.get('/', (req, res) => {
    if (DEBUG) { console.debug("Home page:", req.path); }
    res.sendFile(path.join(__dirname+'/public/maintenance.html'));
});


// Legit go.epfl.ch's pages (they should win over redirect below)
// They return 200 but with the maintenance page
const pages = [ '/about', '/admin', '/admin/*', '/admin-php', '/alias', '/alias/*', 
    '/aliases', '/api', '/api/*', '/atom', '/browse', '/browsedt', '/browsedtajax', 
    '/browsedtnocache', '/contact', '/design', '/edit', '/edit/*', '/extension', 
    '/faq', '/feed', '/grafana', '/info', '/info/*', '/login', '/login/*', 
    '/logout', '/monitoring', '/profile', '/prom', '/prometheus', '/qr', '/qr/*', 
    '/react', '/report', '/report/*', '/reveal', '/reveal/*', '/rss', '/stats', 
    '/token', '/try' ];

app.get(pages, (req, res) => {
    if (DEBUG) { console.debug("Legit page:", req.path); }
    res.sendFile(path.join(__dirname+'/public/maintenance.html'));
});


// All redirect from the exported CSV (existing aliases)
// Please note that this need to be synchronous !
const aliases_file = './alias-url.csv';
const data = fs.readFileSync( aliases_file, { encoding: 'utf8', flag: 'r' } );
data.split( "\n" ).forEach( line => {
    const [ alias, url ] = line.split( "\t" );
    console.log('processing', alias, '→', url)
    app.get(`/${alias}`, ( req, res ) => {
        if (DEBUG) { console.debug("CSV redirect:", req.path); }
        res.redirect(`${url}`);
    });
 });

// Magic redirects
const servicenow_url = 'https://support.epfl.ch';

// magic redirect: .../INC0441402
app.get('/INC:inc([0-9]{7})/', (req, res) => {
    if (DEBUG) { console.debug("Magic redirect:", req.path); }
    res.redirect(`${servicenow_url}/nav_to.do?uri=/incident.do?sysparm_query=number=INC${req.params.inc}`);
});

// magic redirect: .../CHG0040706
app.get('/CHG:chg([0-9]{7})/', (req, res) => {
    if (DEBUG) { console.debug("Magic redirect:", req.path); }
    res.redirect(`${servicenow_url}/nav_to.do?uri=/change_request.do?sysparm_query=number=CHG${req.params.chg}`);
});

// magic redirect: .../CIM0001553
app.get('/CIM:cim([0-9]{7})/', (req, res) => {
    if (DEBUG) { console.debug("Magic redirect:", req.path); }
    res.redirect(`${servicenow_url}/nav_to.do?uri=/sn_cim_register.do?sysparm_query=number=CIM{req.params.cim}`);
});

// magic redirect: .../CTASK0019229
app.get('/CTASK:ctask([0-9]{7})/', (req, res) => {
    if (DEBUG) { console.debug("Magic redirect:", req.path); }
    res.redirect(`${servicenow_url}/nav_to.do?uri=/change_task.do?sysparm_query=number=CTASK${req.params.ctask}`);
});

// magic redirect: .../OUT0006519
app.get('/OUT:out([0-9]{7})/', (req, res) => {
    if (DEBUG) { console.debug("Magic redirect:", req.path); }
    res.redirect(`${servicenow_url}/nav_to.do?uri=/cmdb_ci_outage.do?sysparm_query=u_number=OUT${req.params.out}`);
});

// magic redirect: .../KB0015277
app.get('/KB:kb([0-9]{7})/', (req, res) => {
    if (DEBUG) { console.debug("Magic redirect:", req.path); }
    res.redirect(`${servicenow_url}/epfl?id=epfl_kb_article_view&sysparm_article=KB${req.params.kb}`);
});

// magic redirect: .../bKB0015277
app.get('/bKB:kb([0-9]{7})/', (req, res) => {
    if (DEBUG) { console.debug("Magic redirect:", req.path); }
    res.redirect(`${servicenow_url}/kb_view.do?sysparm_article=KB${req.params.kb}`);
});

// magic redirect: .../RITM0246550
app.get('/RITM:ritm([0-9]{7})/', (req, res) => {
    if (DEBUG) { console.debug("Magic redirect:", req.path); }
    res.redirect(`${servicenow_url}/nav_to.do?uri=/sc_req_item.do?sysparm_query=number=RITM${req.params.ritm}`);
});

// magic redirect: .../IDEA0001152
app.get('/IDEA:idea([0-9]{7})/', (req, res) => {
    if (DEBUG) { console.debug("Magic redirect:", req.path); }
    res.redirect(`${servicenow_url}/nav_to.do?uri=/idea.do?sysparm_query=number=IDEA${req.params.idea}`);
});

// magic redirect: .../PRB0040262
app.get('/PRB:prb([0-9]{7})/', (req, res) => {
    if (DEBUG) { console.debug("Magic redirect:", req.path); }
    res.redirect(`${servicenow_url}/nav_to.do?uri=/problem.do?sysparm_query=number=PRB${req.params.prb}`);
});

// magic redirect: .../SVC0176
app.get('/SVC:svc([0-9]{4})/', (req, res) => {
    if (DEBUG) { console.debug("Magic redirect:", req.path); }
    res.redirect(`${servicenow_url}/nav_to.do?uri=/cmdb_ci_service.do?sysparm_query=u_number=SVC${req.params.svc}`);
});

// magic redirect: .../RSK0000074
app.get('/RSK:rsk([0-9]{7})/', (req, res) => {
    if (DEBUG) { console.debug("Magic redirect:", req.path); }
    res.redirect(`${servicenow_url}/nav_to.do?uri=x_epfl2_audit_risk.do?sysparm_query=number=RSK${req.params.rsk}`);
});

// magic redirect: .../AUD0000001
app.get('/AUD:aud([0-9]{7})/', (req, res) => {
    if (DEBUG) { console.debug("Magic redirect:", req.path); }
    res.redirect(`${servicenow_url}/nav_to.do?uri=x_epfl2_audit_report.do?sysparm_query=number=AUD${req.params.aud}`);
});


// Everything else return the maintenance
app.get('*', (req, res, next) => {
    if (DEBUG) { console.debug("* redirect:", req.path); }
    res.status(503).sendFile(path.join(__dirname+'/public/maintenance.html'));
});
