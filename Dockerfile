FROM node:18.12
# ARG datapath="/var/lib/postgresql/data/alias-url.csv"
WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install

COPY public ./public
COPY index.js ./
# COPY ${datapath} ./alias-url.csv

EXPOSE 3000
CMD [ "node", "index.js" ]
