#!/usr/bin/env bash
#
# Usage: BASE_URL=https://go-tst.idev-fsd.ml ./test.sh
#
#set -e -x

# A small script to validate the redirection
RED='\033[0;31m'
GREEN='\033[1;32m'
NC='\033[0m' # No Color

BASE_URL=${BASE_URL:-'http://localhost:3000'}

redirect () {
    curl -I -s -w "%{http_code} %{redirect_url}\\n" "$BASE_URL/$1" -o /dev/null
}

redirectXXX () {
    REDIRECT=$(redirect $1)
    HTTP_STATUS=$(echo $REDIRECT | cut -d ' ' -f1)
    REDIRECT_URL=$(echo $REDIRECT | cut -d ' ' -f2)
    if [[ $HTTP_STATUS = $REDIRECT_URL ]] ; then
        REDIRECT_URL="$BASE_URL/$1 (not redirected)"
    fi
    if [[ $HTTP_STATUS = $2 ]] ; then
        printf "${GREEN}$HTTP_STATUS${NC} %-20s → $REDIRECT_URL\n" $1
    else
        printf "${RED}$HTTP_STATUS${NC} %-20s → $REDIRECT_URL\n" $1
    fi
}

redirectBase () {
    redirectXXX '' 200
}

redirect200 () {
    redirectXXX $1 200
}

redirect302 () {
    redirectXXX $1 302
}

redirect503 () {
    redirectXXX $1 503
}

# Testing 302
All302Alias=('INC0441402' 'CHG0040706' 'CTASK0019229' 'OUT0006519' 'KB0015277' 'bKB0015277' 'RITM0246550' 'IDEA0001152' 'PRB0040262' 'SVC0176' 'RSK0000074' 'AUD0000001' 'nbo' 'paprika' 'coronavirus' 'privacy-policy' 'aYowU' 'UNIL-118' 'EdT0L' 'zombo' 'zzzz')
for alias in ${All302Alias[@]}; do
    redirect302 $alias
done

# Testing 503
All503Alias=('inc0441402' 'thisdoesntexist')
for alias in ${All503Alias[@]}; do
    redirect503 $alias
done

# Testing 200
All200Alias=('about' 'admin' 'admin/*' 'admin-php' 'alias' 'alias/test'
    'aliases' 'api' 'api/test' 'atom' 'browse' 'browsedt' 'browsedtajax'
    'browsedtnocache' 'contact' 'design' 'edit' 'edit/*' 'extension'
    'faq' 'feed' 'grafana' 'info' 'info/test' 'login' 'login/test'
    'logout' 'monitoring' 'profile' 'prom' 'prometheus' 'qr' 'qr/test'
    'react' 'report' 'report/test' 'reveal' 'reveal/test' 'rss' 'stats'
    'token' 'try')
for alias in ${All200Alias[@]}; do
    redirect200 $alias
done

redirectBase
