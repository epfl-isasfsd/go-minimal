docker exec -it go_postgres psql -U postgres -d goepfl -t -c "copy (select alias, url from aliases left join urls on aliases.url_id=urls.id where aliases.obsolescence_date>CURRENT_DATE or aliases.obsolescence_date is null) to '/var/lib/postgresql/data/alias-url.csv'"
docker exec -it go_postgres chmod 755 /var/lib/postgresql/data/alias-url.csv
sudo chmod o+rx /srv/go.epfl.ch/data
sudo chmod o+rx /srv/go.epfl.ch/data/postgres
cat /srv/go.epfl.ch/data/postgres/alias-url.csv
cp /srv/go.epfl.ch/data/postgres/alias-url.csv .
