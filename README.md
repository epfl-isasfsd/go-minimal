# Go Minimal

Go is the URL shortener for the [EPFL] community. This project aims to create a 
**minimalist redirection system** that would allow users to continue using Go 
links **while the system is under maintenance**.


## Goal

The goal is to have a fast-deployable and reliable (>100 requests second) system.

There are 3 objectifs for this projet:
   1. Extract data (Alias → URL) from the database automatically;
   1. Create the redirect system ([302]), based on the key-value extracted in 1.;
   1. Provide the HTTP web server that will handle the request.

Please note that all URLs that are not an alias should lead to the same 
"under maintenance" ([503]) static page.


[##]: //
[EPFL]: https://www.epfl.ch
[302]: https://en.wikipedia.org/wiki/HTTP_302
[503]: https://en.wikipedia.org/wiki/HTTP_503
